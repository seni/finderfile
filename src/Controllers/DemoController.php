<?php

namespace Seni\FileFinder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;

class DemoController extends Controller
{
    public $accessibleDirectories = ['shared', 1];

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $files = File::allAccessibleFiles($this->accessibleDirectories);
        if (empty($files)) {
            $files = [];
        }
        return view('filefinder::index', compact('files'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchFile(Request $request)
    {
        $keyword = $request->input('search');
        $accessibleFiles = File::allAccessibleFiles($this->accessibleDirectories);

        if(!$keyword){
            return back()->with(['msg' => 'MSG']);
        }

        $files = File::searchByKeyword($accessibleFiles, $keyword);

        return view('filefinder::index', ['files' => $files]);
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $uploadedFile = $request->file('upload_file');

        if (!$uploadedFile){
            return back()->with(['msg' => 'MSG']);
        }

        $filename = $uploadedFile->getClientOriginalName();

        File::storeFile($uploadedFile, $filename, $userFileDir = '1', $disk = 'public');

        Session::flash('message', "Special message goes here");

        return back()->with('msg', 'MSG');

    }

    /**
     * @param Request $request
     * @return array
     */
    public function download(Request $request)
    {
        $filePath = str_replace('public', '', $request->get('filePath'));

        return File::downloadFile($filePath);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function destroy(Request $request)
    {
        $filePath = str_replace('public', '', $request->get('filePath'));

        File::deleteFile($filePath);

        return redirect('files');
    }

}