<?php

use Illuminate\Support\Facades\Route;

Route::get('files', '\Seni\FileFinder\DemoController@index');
Route::post('files', '\Seni\FileFinder\DemoController@store')->name('files.store');
Route::any('files/search', '\Seni\FileFinder\DemoController@searchFile')->name('files.searchFile');
Route::get('files/download/', '\Seni\FileFinder\DemoController@download')->name('files.download');
Route::delete('files/delete/', '\Seni\FileFinder\DemoController@destroy')->name('files.destroy');
