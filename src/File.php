<?php

namespace Seni\FileFinder;

use Illuminate\Support\Facades\Storage;

class File
{
    
    /**
     * @param array $accessibleDirectories
     * @param string $disk
     * @return mixed
     */
    public static function allAccessibleFiles($accessibleDirectories = [], $disk = 'public')
    {
        $basePath = $disk . '/files/';
        $accessibleFiles = [];
        $files = [];

        if(!empty($accessibleDirectories) && is_array($accessibleDirectories)) {
            foreach($accessibleDirectories as $dir){
                $is_exists = Storage::disk($disk)->exists('files/' . $dir);
                if($is_exists){
                    $accessibleFiles[] = $basePath . (string) $dir;
                }
            }

            foreach (SearchGenerator::generateListWithAccessibleFiles($accessibleFiles, count($accessibleFiles)) as $rangeFile) {
                if(is_array($rangeFile) && empty($rangeFile)){
                    return $files = [];
                }

                foreach ($rangeFile as $value){
                    $files[] = $value;
                }
            }

        } else {
            $files = Storage::allFiles($basePath);
        }

        return $files;
    }

    /**
     * @param $files
     * @param $keyword
     * @return array
     */
    public static function searchByKeyword($files, $keyword)
    {
        $searchedFiles = [];

        foreach (SearchGenerator::generateListWithSearchedFiles($files, $keyword, count($files)) as $result){
            $searchedFiles[] = $result;
        }

        return $searchedFiles;
    }

    /**
     * @param object $uploadedFile
     * @param string $filename
     * @param string $disk
     * @param string $userFileDir
     * @return array
     */
    public static function storeFile($uploadedFile, $filename, $userFileDir = 'shared', $disk = 'public') : array
    {
        try{

            Storage::disk($disk)->putFileAs(
                'files/'. $userFileDir,
                $uploadedFile,
                $filename
            );

            $response = ['success' => 'File Successfully Uploaded'];

            return $response;

        } catch (\Exception $exception){
            $response = ['errors' => $exception->getCode() . ' ' . $exception->getMessage()];
            return $response;
        }
    }

    /**
     * @param $filepath
     * @param string $disk
     * @return array
     */
    public static function downloadFile($filepath, $disk = 'public')
    {
        try{

            return Storage::disk($disk)->download($filepath);

        } catch (\Exception $exception){
            $response = ['errors' => $exception->getCode() .' '. $exception->getMessage()];
            return $response;
        }
    }

    /**
     * @param $filepath
     * @param string $disk
     * @return array
     */
    public static function deleteFile($filepath, $disk = 'public') : array
    {
        try{
            $is_exists = Storage::disk($disk)->exists($filepath);
            if($is_exists){
                Storage::disk($disk)->delete($filepath);
                $response = ['success' => 'File Successfully Deleted'];

                return $response;
            }

            return [];

        } catch (\Exception $exception){
            $response = ['errors' => $exception->getCode() .' '. $exception->getMessage()];
            return $response;
        }
    }
}