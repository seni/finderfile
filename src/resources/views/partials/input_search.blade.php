<div class="input-group col-10 {{ $customClassesForSearchWrapper ?? '' }}">
    <input type="text" id="{{$searchId ?? 'search'}}" name="{{ $searchName ?? 'search' }}" class="form-control {{ $customClassesForSearchInput ?? '' }}"
           placeholder="{{ $customSearchPlaceholder ?? '' }}">
    <div class="input-group-append">
        <button class="btn {{ $submitButtonClasses ?? '' }}" type="submit">
            {{ $submitButtonText ?? 'Search' }}
        </button>
    </div>
</div>