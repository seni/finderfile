<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text p-0">
            <button class="btn {{ $submitButtonClasses ?? '' }}" type="submit">{{ $submitButtonText ?? 'Submit' }}</button>
        </span>
    </div>
    <div class="custom-file {{ $customClassesForFileWrapper ?? '' }}">
        <input type="file"
               name="{{$inputName ?? 'upload_file'}}"
               class="custom-file-input {{ $customClassesForInput ?? '' }}"
               id="{{ $customIdForInput ?? '' }}"
               accept="{{ $fileExtensions ?? '.txt' }}"
                @if($additionalDataAttributes)
                    @foreach($additionalDataAttributes as $key => $value)
                        {{ $key . '=' . $value }}
                    @endforeach
                @endif
                {{ $additionalDataAttribute ?? '' }} />
        <label class="custom-file-label {{ $customClassesForLabel ?? '' }}" for="{{$inputName ?? 'upload_file'}}">{{ $customLabelText ?? '' }}</label>
    </div>
</div>