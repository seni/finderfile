<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<div class="container my-5">
    <div class="row">
        <div class="col-6">
            <h5 class="pl-3">Search</h5>
            <form action="{{route('files.searchFile')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                @include('filefinder::partials.input_search',
                 ['submitButtonClasses' => 'btn-info'])
            </form>
        </div>
        <div class="col-6">
            <h5 class="pl-3">Upload file</h5>
            <form action="{{route('files.store')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="col-10">
                    @include('filefinder::partials.input_type_file',
                     ['submitButtonText' => 'Submit', 'submitButtonClasses' => 'btn-success',
                     'fileExtensions' => '.txt, .odt',
                      'additionalDataAttributes' => ['data1' => 'data1']])
                </div>
            </form>

        </div>
    </div>
    <div class="row">
        <table class="table table-sm">
            <thead>
            <tr class="table-info">
                <th scope="col">File Name</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
                @if(!$files)
                <tr>
                    <td>
                        No data available
                    </td>
                </tr>
                @endif
                @foreach($files as $file)
                    <tr>
                        <td>
                            {{ basename($file) }}
                        </td>
                        <td>
                            <a href="{{url('files/download?filePath=' . urlencode($file))}}" class="btn btn-success d-inline-block">Download</a>
                            <form action="{{url('files/delete')}}" method="post" enctype="multipart/form-data" class="d-inline-block">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <input type="hidden" name="filePath" value="{{$file}}">
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>


</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>