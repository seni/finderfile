<?php

namespace Seni\FileFinder;

use Illuminate\Support\Facades\Storage;

class SearchGenerator{

    /**
     * @param array $accessibleFiles
     * @param int $max
     * @return \Generator
     */
    public static function generateListWithAccessibleFiles($accessibleFiles, $max = 10){
        for ($i = 0; $i < $max; $i++) {

            $files = Storage::allFiles($accessibleFiles[$i]);

            if($files){
                yield $files;
            }
        }
    }

    /**
     * @param array $files
     * @param string $keyword
     * @param int $max
     * @return \Generator
     */
    public static function generateListWithSearchedFiles($files, $keyword, $max = 10)
    {
        for ($i = 0; $i < $max; $i++) {
            $file = Storage::get($files[$i]);
            if (strpos($file, $keyword) !== false) {
                yield $files[$i];
            }
        }
    }
}