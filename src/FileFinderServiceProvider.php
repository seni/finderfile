<?php


namespace Seni\FileFinder;


use Illuminate\Support\ServiceProvider;

class FileFinderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/resources/views/', 'filefinder');
    }


    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function register()
    {
        $this->app->make('Seni\FileFinder\DemoController');
        include __DIR__ . '/routes.php';
    }
}