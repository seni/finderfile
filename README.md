# File Finder



### Getting started

You must first install the package. Please run the command:

`composer require seni/filefinder`

You must to add the package service provider to your project:
 
 `'providers' => [ ...
    Seni\FileFinder\FileFinderServiceProvider::class, .....]` to 
    `YourAppName/config/app.php`
    
### Backend of the package

Create a new controller. 
Add a package to your controller

`use Seni\FileFinder\File;`

Available methods:
 + `File::allAccessibleFiles([value, ...]);`
 + `File::searchByKeyword($accessibleFiles, $keyword);`
 + `File::storeFile($uploadedFile, $filename, $userFileDir = 'shared', $disk = 'public');`
 + `File::downloadFile($filepath);`
 + `File::deleteFile($filepath);`
 
 

In the index method, you can use 

`File::allAccessibleFiles([value, ...]);`, as parameters the method expect
accessible directory/ies and disk('local' / 'public').
The accessible directory/ies is array of directory/directories which you give
the users to access. You can define global variable,
that you can use in the controller.
The disk have a default value - public, if you want you can change it.

Example:

`public $accessibleDirectories = ['shared', 1];`

`File::allAccessibleFiles($this->accessibleDirectories);` 

The method will return array of files that are accessible.

----------------------------------------------------------

The method `File::searchByKeyword($accessibleFiles, $keyword)` you can use to find 
file/s that contain the searchable keyword. As parameters the method expects 
array of accessible file and keyword - string.
The method return array of file/files, that respond.

Example: 
``` 
    $keyword = $request->input('search');
    $accessibleFiles = File::allAccessibleFiles($this->accessibleDirectories);
    $files = File::searchByKeyword($accessibleFiles, $keyword); 
```
           
-----------------------------------------------------------
    
The other available method is 
`File::storeFile($uploadedFile, $filename, $userFileDir = 'shared', $disk = 'public');`
The method stores the files to the store folder of your project. As parameters, 
the method expects `$uploadedFile` that is object, `$filename` that is string, 
`$userFileDir` that is string. By default `$userFileDir` is shared folder.
 If you want to store files to other folder (`ex. $userId, $username`),
 you can change directory. And the last parameter is `$disk`,
 by default is public directory.
 
 Example:
 ```
 $uploadedFile = $request->file('upload_file');
 $filename = $uploadedFile->getClientOriginalName();
 File::storeFile($uploadedFile, $filename, $userId, $disk = 'public');
```

-----------------------------------------------------------

The package gives you option to download file with 
`File::downloadFile($filepath, $disk = 'public')`.
To use it you must to set file path as parameter. You also can change `$disk` value.

Example:
```
    $filePath = '/files/shared/fileName.txt';
    return File::downloadFile($filePath);
```

-----------------------------------------------------------

And of course you can use delete file method from package. The method is 
`File::deleteFile($filePath);`. It work same as 
`File::downloadFile($filepath, $disk = 'public')`. You also can change `$disk` value. 

Example:
```
    $filePath = '/files/shared/fileName.txt';
    return File::deleteFile($filePath);
```
    
### UI of the package    
You can use the package blade as you add 
`return view('filefinder::index', ['files' => $files])`;
    
#### Upload file /Input Type File/
To use the partial of the package, you need to use the following snippet 
`@include('filefinder::partials.input_type_file')`

When you use upload file component, you can add custom:
 + **Button "Submit uploaded file"** 
    + class to the button - `$submitButtonClasses` - **string**
    + text to the button - `$submitButtonText` - **string**
 + **Input "Upload File"** 
    + name - `$inputName` - **string**
    + file extensions - `$fileExtensions` - **string**
    + class to the component wrapper - `$customClassesForFileWrapper`- **string**
    + class to the input type file - `$customClassesForInput`- **string**
    + id to the input type file - `$customIdForInput`- **string**
    + class to the label - `$customClassesForLabel` - **string**
    + text to the label - `$customLabelText` - **string**
    + additional data attributes - `$additionalDataAttributes` - **array**
 
 Example: 
```
    @include('filefinder::partials.input_type_file',
            ['customClassesForInput' => 'btn btn-default',
             'additionalDataAttributes' => ['dataAttribute' => 'value']
             ]
         )
```
**!NB You can create your custom component**

#### Upload file /Input Type File/
To use the partial of the package, you need to use the following snippet 
`@include('filefinder::partials.input_search')`

When you use search field, you can add custom:
 + **Search input**
    + component wrapper - `$customClassesForSearchWrapper` - **string** 
    + name - `$searchName / default name: search` - **string**
    + class for the input - `$customClassesForSearchInput` - **string**
    + placeholder for the input -`$customSearchPlaceholder` - **string**
 + **Search button**
    + class for the button - `$submitButtonClasses` - **string** 
    + text for the button

 Example: 
```
    @include('filefinder::partials.input_search',
            ['searchName' => 'search_field',
             'submitButtonClasses' => 'btn btn-default'
             ]
         )
```

**!NB You can create your custom component**